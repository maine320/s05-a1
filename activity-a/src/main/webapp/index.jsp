<!--  Import in JSP can be achieved by using directives -->
<!--  Directives are defined by the "%@" symbol which is used to define page attributes -->
<!--  Multiple import packages can be achieved by adding a comma (e.g. import="java.util.Date", java.util.* -->
<%@ page language="java" 
	contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	import="java.util.Date"
	import="java.time.ZonedDateTime"
    import="java.time.ZoneId"
    import="java.time.format.DateTimeFormatter"
    import="java.time.format.FormatStyle"
    %>
     
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Server Pages</title>
</head>
<body>
	<h1>Our Date and Time now is...</h1>
	<!--  JSP allows integration of HTML tags with java syntax -->
	
	<!--  Prints a message in the console -->
	<% System.out.println("Hello from JSP"); %>
	
	<!--  Creates a variable currentDateTime variable -->
	<%
		ZonedDateTime dateTime = ZonedDateTime.now();
	%>
	
	<!-- variable dateTime -->
	
	<%!
		ZonedDateTime dateTime = ZonedDateTime.now();
	%>
	
	<!-- Getting different timezones from different places -->
	
	<%
		ZonedDateTime mnla = dateTime.withZoneSameInstant(ZoneId.of("Asia/Manila"));
	%>
	
	<%
		ZonedDateTime jpn = dateTime.withZoneSameInstant(ZoneId.of("Asia/Tokyo"));
	%>
	
	<%
		ZonedDateTime grmny = dateTime.withZoneSameInstant(ZoneId.of("Europe/Berlin"));
	%>
	
	<!-- DateTimeFormatter to format the date and time -->
	
	<%
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	%>
	
	<%
		String mnlaTime = mnla.format(formatter);
	%>
	
	<%
		String jpnTime = jpn.format(formatter);
	%>
	
	<%
		String grmnyTime = grmny.format(formatter);
	%>
	
	<!-- Display the time and date from different timezones -->
	<ul>
	<li>Manila: <%= mnlaTime %></li>
	<li>Japan: <%= jpnTime %></li>
	<li>Germany: <%= grmnyTime %></li>
	</ul>
	<!--  JSP Declaration -->
	<!--  Allows declaration one or more variables and methods -->
	<%!
		private int initVar = 1;
		private int serviceVar = 1;
		private int destroyVar = 3;
	%>
	
	<!--  JSP method declaration -->
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init" +initVar);
		
		}
	
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy" +destroyVar);
		}
		
	%>
	
	<!-- JSP Scriplets -->
	<!-- Allows any Java language statement, variable, method declaration and expression -->
	<% 
		serviceVar++;
		System.out.println("jspService(): service" +serviceVar);
		String content1 = "content1: " + initVar;
		String content2 = "content2: " + serviceVar;
		String content3 = "content3: " + destroyVar;
	%>
	
	<!-- JSP expressions -->
	<!-- Code placed within the JSP expressions tag is written to the output stream of the response -->
	<!-- out.println is no longer required to print values of variables/methods -->
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>
	
	
</body>
</html>